package timeLoop

import (
	"time"
)

type TimeLoop struct {
	loopFn             func()
	initFn             func() error
	stopFn             func() error
	loopCh             chan bool
	stopCh             chan bool
	loopPeriod         time.Duration
	initFailRetry      bool
	stopFailRetry      bool
	initFailLoopPeriod time.Duration
	stopFailLoopPeriod time.Duration
}

func NewTimeLoop(loopFn func(), timePeriod time.Duration) *TimeLoop {
	return &TimeLoop{
		loopFn:             loopFn,
		loopCh:             make(chan bool, 2),
		stopCh:             make(chan bool, 1),
		loopPeriod:         timePeriod,
		initFailRetry:      true,
		stopFailRetry:      true,
		initFailLoopPeriod: time.Second,
		stopFailLoopPeriod: time.Second,
	}
}

func (t *TimeLoop) Start() {
	t.initFunction()
	for {
		select {
		case <-t.stopCh:
			t.stopFunction()
			return
		default:
			go t.loopFunction()
			go t.timer()
			<-t.loopCh
			<-t.loopCh
		}
	}
}

func (t *TimeLoop) StopLoop() {
	t.stopCh <- false
}

func (t *TimeLoop) SetLoopPeriod(period time.Duration) {
	t.loopPeriod = period
}

func (t *TimeLoop) SetInitialFunc(initFn func() error) {
	t.initFn = initFn
}

func (t *TimeLoop) SetStopFunc(stopFn func() error) {
	t.stopFn = stopFn
}

func (t *TimeLoop) StopLoopInitialFuncOnFail() {
	t.initFailRetry = false
}

func (t *TimeLoop) StopLoopStopFuncOnFail() {
	t.initFailRetry = false
}

func (t *TimeLoop) LoopInitialFuncOnFail(loopPeriod time.Duration) {
	t.initFailLoopPeriod = loopPeriod
	t.initFailRetry = true
}

func (t *TimeLoop) LoopStopFuncOnFail(loopPeriod time.Duration) {
	t.stopFailLoopPeriod = loopPeriod
	t.stopFailRetry = true
}

func (t *TimeLoop) initFunction() {
	if t.initFn == nil {
		return
	}
	var err error
	for err = t.initFn(); err != nil && t.initFailRetry && t.initFn != nil; err = t.initFn() {
		time.Sleep(t.initFailLoopPeriod)
	}
}

func (t *TimeLoop) stopFunction() {
	if t.stopFn == nil {
		return
	}
	var err error
	for err = t.stopFn(); err != nil && t.stopFailRetry && t.stopFn != nil; err = t.initFn() {
		time.Sleep(t.stopFailLoopPeriod)
	}
}

func (t *TimeLoop) loopFunction() {
	t.loopFn()
	t.loopCh <- false
}

func (t *TimeLoop) timer() {
	time.Sleep(t.loopPeriod)
	t.loopCh <- false
}
